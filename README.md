
## React native todo list

Creado en Meetup React Native Live Coding del Java User Group Chile el 25 de Julio de 2018 en axity https://www.meetup.com/jug-chile/events/252754055/.

Para ejecutarlo, 

git clone https://gitlab.com/hanspoo/todo  
cd todo  
npm i  
yarn start  

This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

