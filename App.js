/* eslint no-unused-vars: 0 */
import React from "react";
import { StyleSheet, Text, View, StatusBar } from "react-native";
import SearchBar from "./src/components/SearchBar";
import TodosList from "./src/components/TodosList";
import AddTodoForm from "./src/components/AddTodoForm";

export default class App extends React.Component {
  state = {
    filtro: "",
    todos: [
      { id: 1, tarea: "Estudiar React Native", listo: true },
      { id: 2, tarea: "Cortar el pasto", listo: false },
      { id: 3, tarea: "Ir a la escuela", listo: true }
    ]
  };

  onAdd = tarea => {
    const { todos } = this.state;
    const newTodo = { id: todos.length + 1, listo: false, tarea };
    this.setState({ todos: todos.concat(newTodo) });
  };

  onSearch = filtro => {
    this.setState({ filtro: filtro.toLowerCase() });
  };

  render() {
    const { todos, filtro } = this.state;

    const finalTodos = todos.filter(
      t => t.tarea.toLowerCase().indexOf(filtro) !== -1
    );

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Todos</Text>
        <SearchBar onSearch={this.onSearch} />
        <TodosList todos={finalTodos} />
        <AddTodoForm onAdd={this.onAdd} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: StatusBar.currentHeight
  },
  header: {
    fontSize: 24
  }
});
