import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 7,
    margin: 7
  },
  field: {
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 7,
    margin: 7
  }
});

export default styles;
