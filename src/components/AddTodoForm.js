import React from "react";
import { TextInput, View, Button } from "react-native";
import styles from "../styles";

export default class SearchBar extends React.Component {
  state = { todo: "" };

  onAdd = () => {
    this.props.onAdd(this.state.todo);
    this.setState({ todo: "" });
  };

  onChangeText = todo => {
    this.setState({ todo });
  };
  render() {
    const { todo } = this.state;

    return (
      <View style={styles.field}>
        <View style={styles.field}>
          <TextInput
            value={todo}
            onChangeText={this.onChangeText}
            underlineColorAndroid="transparent"
            placeholder="Buscar..."
          />
        </View>
        <Button title="Enviar" onPress={this.onAdd} />
      </View>
    );
  }
}
