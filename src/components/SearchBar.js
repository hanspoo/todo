import React from "react";
import { TextInput, View } from "react-native";
import styles from "../styles";

export default class SearchBar extends React.Component {
  onChangeText = e => {
    this.props.onSearch(e);
  };

  render() {
    return (
      <View style={styles.field}>
        <TextInput
          onChangeText={this.onChangeText}
          underlineColorAndroid="transparent"
          placeholder="Buscar..."
        />
      </View>
    );
  }
}
