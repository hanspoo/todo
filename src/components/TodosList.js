import React from "react";
import { Text, View, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import styles from "../styles";

const Tarea = ({ id, tarea, listo }) => (
  <Text style={styles.card} id={id}>
    . {tarea}
  </Text>
);

export default class TodosList extends React.Component {
  static propTypes = {
    todos: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        tarea: PropTypes.string.isRequired,
        listo: PropTypes.bool.isRequired
      }).isRequired
    )
  };

  render() {
    const { todos } = this.props;
    return (
      <View style={styles.container}>
        {todos.map(tarea => <Tarea key={tarea.id} {...tarea} />)}
      </View>
    );
  }
}
